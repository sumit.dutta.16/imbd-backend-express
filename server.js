const express = require("express");
const app = express();
const { json } = require("express");

const PORT = process.env.PORT || 3000;

const cors = require("cors");
const corsOption = { origin: "http://localhost:4200" };
app.use(cors(corsOption));

app.use(express.urlencoded({ extended: true }));
app.use(json());

app.use("/movies", require("./routes/router.js"));

app.listen(PORT, (res) => {
  console.log(`Running on port: ${PORT}`);
});
